import React from 'react';
import Api from '../api/Api.js';
import {
  LineChart
} from 'react-native-chart-kit'
import {
  Text, View, Dimensions
} from 'react-native'

export default class HistoryScreen extends React.Component {
  static navigationOptions = {
    title: 'History',
  };
  state = {
    historyItems:[]
  }
  componentDidMount(){
    Api.returnHistory()
    .then(items => {
      let historyItems = []
      items.map((item) => {
        let dataSet = {}
        dataSet.labels = []
        dataSet.data = []
        Object.keys(item).forEach(function(key) {
          dataSet.date = key
          item[key].forEach(obj => {
            dataSet.labels.push(obj.currency)
            dataSet.data.push(obj.rate)
          })
        });
        historyItems.push(dataSet)
      })
      this.setState({historyItems: historyItems})
    })
  }
  returnGraph(item){
    const graph = (
      <View>
        <Text>
          From: { item.date }
        </Text>
        <LineChart
          data={{
            labels: item.labels,
            datasets: [{
              data: item.data
            }]
          }}
          width={Dimensions.get('window').width} // from react-native
          height={220}
          chartConfig={{
            backgroundColor: '#e26a00',
            backgroundGradientFrom: '#000',
            backgroundGradientTo: '#ffa726',
            decimalPlaces: 2, // optional, defaults to 2dp
            color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
            style: {
              borderRadius: 16
            }
          }}
          bezier
          style={{
            marginVertical: 8,
            borderRadius: 16
          }}
        />
      </View>
    )
    return graph;
  }

  render() {
    if (this.state.historyItems.length === 0) {
      return (
        <Text>No data to show.</Text>
      )
    }
    else {
      for (let i = 0; i < this.state.historyItems.length; i++){
         return this.returnGraph(this.state.historyItems[i])
      }
    }
  }
}
