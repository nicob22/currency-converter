import React from 'react';
import Api from '../api/Api.js';
import {
  ScrollView, StyleSheet, Text, View, FlatList,
} from 'react-native';

export default class RecentScreen extends React.Component {
  static navigationOptions = {
    title: 'Recently entered',
  };
  state = {
    recentConversions:[]
  }

  _returnItems(){
    this.state.recentConversions.map(item => {
      return <Text style={styles.text} key={item}>{item}</Text>
    })
  }
  componentDidMount(){
    Api.returnRecent()
    .then(items => this.setState({recentConversions: items}))
  }
  render() {
    return (
        <ScrollView style={styles.container}>
          <View style={styles.view}>
            <FlatList
            data={this.state.recentConversions}
            keyExtractor={(item) => item}
            renderItem={({item}) => <Text style={styles.item}>{item}</Text>}
          />
          </View>
        </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 15,
    backgroundColor: '#fff',
  },
  view: {
    padding: 15,
    alignItems: 'flex-start',
  },
  text: {
    fontSize: 50,
    padding: 20,
    borderStyle: 'solid',
    borderColor: 'grey'
  },
});
