import React from 'react';
import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  View,
  TextInput,
  AsyncStorage,
} from 'react-native';
import RNPickerSelect from 'react-native-picker-select';
import Api from '../api/Api.js';


export default class HomeScreen extends React.Component {
  state = {
    rates: null,
    value: '1',
    amount: '0',
    loading: false,
    currencyA: null,
    currencyB: null,
  }

  static navigationOptions = {
    header: null,
  };

  componentDidMount() {
    this.setState({ loading: true });
    Api.fetchData()
      .then((response) => {
        this.setState({ rates: response.rates, loading: false });
      });
  }

  _returnLoader() {
    return (
      <Text style={styles.getStartedText}>
      Loading ...
      </Text>
    );
  }

  updateAmount() {
    if (
      this.state.currencyA !== null
    && this.state.currencyB !== null
    && this.state.value !== ''
    ) {
      const currencyArate = parseFloat(this.state.currencyA.rate);
      const currencyBrate = parseFloat(this.state.currencyB.rate);
      const value = parseFloat(this.state.value);
      const amount = parseFloat(currencyBrate * value / currencyArate).toFixed(2);
      this.setState({ amount: `${amount}` });
      const label = `${this.state.currencyA.currency}: ${this.state.value} = ${this.state.currencyB.currency}: ${amount}`;
      AsyncStorage.setItem(label, label);
    }
  }

  updateInput(text) {
    this.setState({ value: text }, () => {
      this.updateAmount();
    });
  }

  _returnPicker(changeCurrency) {
    if (!this.state.rates) {
      return this._returnLoader();
    }

    const picker = (
      <RNPickerSelect
        items={this.state.rates.map(rate => ({ label: rate.currency, value: rate, key: rate.currency }))}
        style={{ height: 100, width: 100 }}
        itemStyle={{ height: 100, width: 100 }}
        placeholder={{ label: changeCurrency }}
        onValueChange={(itemValue) => {
          if (changeCurrency === 'currencyA') {
            this.setState({ currencyA: itemValue }, () => { this.updateAmount(); });
          } else if (changeCurrency === 'currencyB') {
            this.setState({ currencyB: itemValue }, () => { this.updateAmount(); });
          }
        }}
      />
    );

    return picker;
  }


  render() {
    return (
      <View style={styles.container}>
        <ScrollView style={styles.container} contentContainerStyle={styles.contentContainer}>
          <View style={styles.welcomeContainer}>
            <Image
              source={require('../assets/images/converter.jpg')}
              style={styles.welcomeImage}
            />
          </View>

          <View style={styles.getStartedContainer}>

            <Text style={styles.getStartedText}>Let&rsquo; s convert some values:</Text>

            {
              this.state.loading
                ? this._returnLoader()
                : (
                  <View>
                    {this._returnPicker('currencyA')}
                    <TextInput
                      style={styles.textInput}
                      onChangeText={text => this.updateInput(text)}
                      keyboardType="numeric"
                      value={this.state.value}
                    />
                    { this._returnPicker('currencyB')}
                    <TextInput
                      editable={false}
                      selectTextOnFocus={false}
                      style={styles.textInput}
                      keyboardType="numeric"
                      value={this.state.amount}
                    />
                  </View>
                )
             }
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  textInput: {
    height: 60,
    width: 200,
    borderColor: 'gray',
    borderWidth: 0,
    textAlign: 'center',
    fontSize: 23,
    marginBottom: 20,

  },
  getStartedContainer: {
    alignItems: 'center',
    marginHorizontal: 50,
  },
  contentContainer: {
    paddingTop: 30,
  },
  welcomeContainer: {
    alignItems: 'center',
    marginTop: 10,
    marginBottom: 20,
  },
  welcomeImage: {
    width: 100,
    height: 100,
    resizeMode: 'contain',
    marginTop: 3,
    marginLeft: -10,
  },

  tabBarInfoContainer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    ...Platform.select({
      ios: {
        shadowColor: 'black',
        shadowOffset: { height: -3 },
        shadowOpacity: 0.1,
        shadowRadius: 3,
      },
      android: {
        elevation: 20,
      },
    }),
    alignItems: 'center',
    backgroundColor: '#fbfbfb',
    paddingVertical: 20,
  },
  tabBarInfoText: {
    fontSize: 17,
    color: 'rgba(96,100,109, 1)',
    textAlign: 'center',
  },
  navigationFilename: {
    marginTop: 5,
  },

});
