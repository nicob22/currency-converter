import { AsyncStorage } from 'react-native';


const Api = {
  fetchData: () => fetch('https://txf-ecb.glitch.me/rates')
    .then(res => res.json())
    .then((resJson) => {
      AsyncStorage.setItem(resJson.time, JSON.stringify(resJson.rates));
      return resJson;
    }),
    returnRecent: () => {
      return AsyncStorage.getAllKeys()
      .then(keys => keys.filter(key => key.indexOf('=') > 0))
      
    },
    returnHistory: () => {
      var objects = []
      return new Promise((resolve, reject) => {
        AsyncStorage.getAllKeys()
        .then(keys => keys.filter(key => key.indexOf('-') > 0))
        .then(filteredKeys => filteredKeys.map(key => {
        AsyncStorage.getItem(key)
        .then(value => {
          var object = {}
          object[key] = JSON.parse(value);
          objects.push(object)
          resolve(objects);
        })
        }))
        .catch(e => reject(e))
    })
  }
};

module.exports = Api;
